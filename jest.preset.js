// @ts-check

const { workspaceRoot } = require('@nrwl/devkit')
const path = require('path')

const nxPreset = require('@nx/jest/preset').default

/** @type {import('jest').Config} */
module.exports = {
    ...nxPreset,
    moduleNameMapper: {
        '\\.(jpg|jpeg|png|gif|eot|otf|webp|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$': `${workspaceRoot}/mocks/assets.ts`,
        'typesafe-i18n/angular': 'typesafe-i18n/angular/index.cjs',
        'typesafe-i18n/react': 'typesafe-i18n/react/index.cjs',
        'typesafe-i18n/solid': 'typesafe-i18n/solid/index.cjs',
        'typesafe-i18n/svelte': 'typesafe-i18n/svelte/index.cjs',
        'typesafe-i18n/vue': 'typesafe-i18n/vue/index.cjs',
        'typesafe-i18n/formatters': 'typesafe-i18n/formatters/index.cjs',
        'typesafe-i18n/detectors': 'typesafe-i18n/detectors/index.cjs',
    },
    roots: ['<rootDir>', path.resolve(__dirname, './mocks')],
    setupFilesAfterEnv: [`${workspaceRoot}/test.setup.ts`],
}
