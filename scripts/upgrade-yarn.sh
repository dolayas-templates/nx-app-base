#!/usr/bin/env bash

yarn set version stable

YARN_VERSION=$(yarn --version)

volta pin yarn@"${YARN_VERSION}" node
