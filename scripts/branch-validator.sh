#!/usr/bin/env bash
LC_ALL=C

CURRENT_BRANCH="$(git rev-parse --abbrev-ref HEAD)"

BRANCH_GROUPS="feature|fix|release|epic|test"

NAMING_PATTERN="([0-9a-zA-Z]*)(-_[0-9a-zA-Z]+)*(_[0-9a-zA-Z]+)$"

IGNORE_PATTERN="dev|stage|main|master"

VALIDATE_BRANCH_REGEX="^(${BRANCH_GROUPS})\/${NAMING_PATTERN}|${IGNORE_PATTERN}"

NEWLINE=$'\n'

message="There is something wrong with your branch name.${NEWLINE}Branch names in this project must adhere to this contract:${NEWLINE}${VALIDATE_BRANCH_REGEX}.${NEWLINE}Your commit will be rejected.${NEWLINE}You should rename your branch to a valid name and try again."

if [[ ! ${CURRENT_BRANCH} =~ ${VALIDATE_BRANCH_REGEX} ]]; then
    echo "${message}"

    exit 1
fi

echo "Branch naming is OK"

exit 0
