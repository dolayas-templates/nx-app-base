// @ts-check

const {
    utils: { getProjects },
} = require('@commitlint/config-nx-scopes')

/** @type {import('@commitlint/types').UserConfig} */
module.exports = {
    extends: ['@commitlint/config-conventional'],
    rules: {
        'scope-enum': async context => [
            2,
            'always',
            [...(await getProjects(context))],
        ],
    },
}
