// @ts-check
const { withTV } = require('tailwind-variants/transformer')

/** @type  {Partial<import('tailwindcss/types/config').ThemeConfig>} */
const appTheme = {}

/** @type  {import('tailwindcss').Config} */
module.exports = withTV({
    content: [],
    theme: {
        extend: {
            ...appTheme,
        },
    },
    plugins: [],
})
